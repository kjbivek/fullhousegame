package boardgame.gui;

import boardgame.engine.GameBox;
import boardgame.engine.GameEngine;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import java.io.*;

public class Controller implements Initializable {

    private GameEngine game;
    private long startTime;
    private GameButton button;


    @FXML
    GridPane boardGridPane = new GridPane();

    public Controller() {
    }

    /**
     * Start an easy game, Start a 5 x 5 game
     */
    @FXML
    private void beginner(ActionEvent event) {
        game = new GameEngine(5);
       
      game.positionObstacles(1, 3);
      game.positionObstacles(2, 3);
      game.positionObstacles(2, 2);
      game.positionObstacles(4, 0);
      game.positionObstacles(4, 1);

        showBoard();
        startTime = System.currentTimeMillis();

    }

    /**
     * Start a medium game, Start a 6 x 6 game
     */
    @FXML
    private void ameture(ActionEvent event) {
        game = new GameEngine(6);
        game.positionObstacles(1, 3);
        game.positionObstacles(2, 2);
        game.positionObstacles(3, 4);
        game.positionObstacles(5, 3);
        game.positionObstacles(5, 2);

        showBoard();
        startTime = System.currentTimeMillis();

    }

    /**
     * Start a hard game, Start a 7 x 7 game
     */
    @FXML
    private void professional(ActionEvent event) {
        game = new GameEngine(7);
        game.positionObstacles(0, 5);
        game.positionObstacles(0, 6);
        game.positionObstacles(2, 0);
        game.positionObstacles(4, 1);
        game.positionObstacles(4, 3);
        game.positionObstacles(6, 4);

        showBoard();
        startTime = System.currentTimeMillis();
    }

    /**
     * Display the board into the user interface along with buttons and events
     */
    private void showBoard() {
        boardGridPane.getChildren().clear();
        boardGridPane.getColumnConstraints().clear();
        boardGridPane.getRowConstraints().clear();

        // Display the buttons
        for (int r = 0; r < game.getRowSize(); r++) {
            for (int c = 0; c < game.getColumnSize(); c++) {
                button = new GameButton(r, c, (GameBox) game.getValue(r, c));
                boardGridPane.add(button, c, r);
            }
        }

        // Make the buttons equal in height
        for (int row = 0; row < game.getRowSize(); row++) {
            RowConstraints rowConstraints = new RowConstraints();
            rowConstraints.setFillHeight(true);
            rowConstraints.setVgrow(Priority.ALWAYS);
            boardGridPane.getRowConstraints().add(rowConstraints);
        }

        // Make buttons equal in width
        for (int col = 0; col < game.getColumnSize(); col++) {
            ColumnConstraints colConstraints = new ColumnConstraints();
            colConstraints.setFillWidth(true);
            colConstraints.setHgrow(Priority.ALWAYS);
            boardGridPane.getColumnConstraints().add(colConstraints);
        }
    }

    
    private void moveArrowTo(int row, int col) {
        try {
            game.moveArrowTo(row, col);
            showBoard();

            
            if (game.isGameOver()) {
                Alert alert = new Alert(AlertType.INFORMATION);  
                if (game.isSolved()) {
                	alert.setTitle("Winner");
                	alert.setHeaderText("Congratulations you won the game!");
                    long gameTimePeriod = (System.currentTimeMillis() - startTime) / 1000;
                    alert.setContentText("You completed the puzzle in " + gameTimePeriod  + " seconds.");
                } else {
                	alert.setTitle("Game Over!");
                	alert.setHeaderText("Aabhash FullHouse Game says");
                    alert.setContentText("Oops! Try again?");
                }

                alert.showAndWait();
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

/**
 * A game button is a button that is on the board.
 */
class GameButton extends Button implements EventHandler<ActionEvent> {

    private int row;
    private int col;

    /**
     * Initialize the button and present the current state of a grid
     * location in the board
     *
     * @param row Assigned row
     * @param col Assigned column
     */
     GameButton(int row, int col, GameBox gamebox) {

        this.row = row;
        this.col = col;

        setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        setMinSize(50, 50);

        // Button is only clickable if empty
        if (gamebox == GameBox.EMPTY) {
            setOnAction(this);
        } else {
            try {
                ImageView imageView = new ImageView();
                switch (gamebox) {

                    case UP_ARROW:
                    {
                        imageView = new ImageView();
                        Image image=new Image(new FileInputStream("../../upArrow.png"), 50, 50, true, true);
                        imageView.setImage(image);
                        
                        break;
                    }
                    case DOWN_ARROW:
                    {
                        imageView = new ImageView();
                        Image image=new Image(new FileInputStream("../../downArrow.png"), 50, 50, true, true);
                        imageView.setImage(image);
                        
                        break;
                    }
                    case LEFT_ARROW:
                    {
                        imageView = new ImageView();
                        Image image=new Image(new FileInputStream("../../leftArrow.png"), 50, 50, true, true);
                        imageView.setImage(image);
                        
                        break;
                    }
                    case RIGHT_ARROW:
                    {
                        imageView = new ImageView();
                        Image image=new Image(new FileInputStream("../../rightArrow.png"), 50, 50, true, true);
                        imageView.setImage(image);
                        
                        break;
                    }
                    case INITIAL_MARKER:
                    {
                        imageView = new ImageView();
                        Image image=new Image(new FileInputStream("../../initialMarker.png"), 50, 50, true, true);
                        imageView.setImage(image);
                        
                        break;
                    }
                    case STOPPER:
                    {
                    	imageView = new ImageView();
                        Image image=new Image(new FileInputStream("../../initialMarker.png"), 50, 50, true, true);
                        imageView.setImage(image);
                        
                        break;
                    }
                    default:
                        break;
                }

                imageView.setFitWidth(50);
                imageView.setFitHeight(50);
                setGraphic(imageView);
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        }
    }

    /**
     * Apply a move to the board and update the board
     *
     * @param event Action event
     */
    @Override
    public void handle(ActionEvent event) {
        moveArrowTo(row, col);
    }
}
}
