package boardgame.engine;

/**
 * A GameBox represents where markers can be placed
 */
public enum GameBox {
    EMPTY("e"),
    LEFT_ARROW("l"),
    RIGHT_ARROW("r"),
    UP_ARROW("u"),
    DOWN_ARROW("d"),
    INITIAL_MARKER("i"),
    STOPPER("s");

    private String str;

    /**
     * Initialize the string representation of a GameBox
     *
     * @param str String representation
     */
    GameBox(String str) {
        this.str = str;
    }

    /**
     * Return the string representation of a GameBox
     *
     * @return string
     */
    @Override
    public String toString() {
        return str;
    }
}
