package boardgame.engine;

public class GameEngine extends GameBoard<GameBox> {

    private boolean initialflag;
    private int movetolastRow;
    private int movetolastCol;

    /**
     * Creates a square game board.
     *
     * @param size the width and height.
     * @throws RuntimeException if the size is invalid
     */
    public GameEngine(int size) {
        super(size, size);

        for (int r = 0; r < getRowSize(); r++) {
            for (int c = 0; c < getColumnSize(); c++) {
                setValue(r, c, GameBox.EMPTY);
            }
        }

        initialflag = false;
        movetolastRow = -1;
        movetolastCol = -1;
    }

    /**
     * Make a move to the board. If this is the first move, then the spot is
     * marked. Otherwise, it will make straight direction depending on the last
     * mark position.
     *
     * @param row Target row
     * @param col Target column
     * @throws IllegalArgumentException if the selected row and column is not
     * within the board or the chosen location can't be marked
     */
    public void moveArrowTo(int row, int col) {
        if (row < 0 || row >= getRowSize() || col < 0 || col >= getColumnSize()
                || getValue(row, col) != GameBox.EMPTY) {
            throw new IllegalArgumentException();
        }

        // Place a new marker
        if (!initialflag) {
            setValue(row, col, GameBox.INITIAL_MARKER);
            initialflag = true;

            movetolastRow = row;
            movetolastCol = col;
            return;
        }

        // Check if we're going up
        if (row < movetolastRow) {
            for (movetolastRow--; movetolastRow >= 0 && getValue(movetolastRow, movetolastCol) == GameBox.EMPTY; movetolastRow--) {
                setValue(movetolastRow, movetolastCol, GameBox.UP_ARROW);
            }

            movetolastRow++;
            return;
        }

        // Check if we're going down
        if (row > movetolastRow) {
            for (movetolastRow++; movetolastRow < getRowSize() && getValue(movetolastRow, movetolastCol) == GameBox.EMPTY; movetolastRow++) {
                setValue(movetolastRow, movetolastCol, GameBox.DOWN_ARROW);
            }

            movetolastRow--;
            return;
        }

        // Check if we're going to the left
        if (col < movetolastCol) {
            for (movetolastCol--; movetolastCol >= 0 && getValue(movetolastRow, movetolastCol) == GameBox.EMPTY; movetolastCol--) {
                setValue(movetolastRow, movetolastCol, GameBox.LEFT_ARROW);
            }

            movetolastCol++;
            return;
        }

        // Check if we're going to the right
        if (col > movetolastCol) {
            for (movetolastCol++; movetolastCol < getColumnSize() && getValue(movetolastRow, movetolastCol) == GameBox.EMPTY; movetolastCol++) {
                setValue(movetolastRow, movetolastCol, GameBox.RIGHT_ARROW);
            }

            movetolastCol--;
            return;
        }

        throw new IllegalArgumentException();
    }

    /**
     * Check if the game is over, that is if there are no other possible moves
     * available
     *
     * @return True if game over, otherwise false
     * @throws RuntimeException if the game hasn't started yet
     */
    public boolean isGameOver() {
        if (!initialflag) {
            throw new RuntimeException();
        }

        // Check all possible directions if there's a next possible move
        if (movetolastRow - 1 >= 0 && getValue(movetolastRow - 1, movetolastCol) == GameBox.EMPTY) {
            return false;
        }

        if (movetolastRow + 1 < getRowSize() && getValue(movetolastRow + 1, movetolastCol) == GameBox.EMPTY) {
            return false;
        }
        if (movetolastCol - 1 >= 0 && getValue(movetolastRow, movetolastCol - 1) == GameBox.EMPTY) {
            return false;
        }

        if (movetolastCol + 1 < getColumnSize() && getValue(movetolastRow, movetolastCol + 1) == GameBox.EMPTY) {
            return false;
        }

        return true;
    }

    /**
     * Check if the game has been solved, that is if all GameBoxs are occupied.
     *
     * @return True if solved, otherwise false
     */
    public boolean isSolved() {
        for (int r = 0; r < getRowSize(); r++) {
            for (int c = 0; c < getColumnSize(); c++) {
                if (getValue(r, c) == GameBox.EMPTY) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Mark a GameBox in the board as a stopper. A stopper is something that stops
     * the flow of a direction on the game.
     *
     * @param row Target row
     * @param col Target column
     * @throws IllegalArgumentException if the row and columns is not within the
     * board or is not empty.
     */
    public void positionObstacles(int row, int col) {
        if (row < 0 || row >= getRowSize() || col < 0 || col >= getColumnSize()
                || getValue(row, col) != GameBox.EMPTY) {
            throw new IllegalArgumentException();
        }

        setValue(row, col, GameBox.STOPPER);
    }

    /**
     * Return the last row move
     *
     * @return Last row move
     * @throws RuntimeException if there is no move.
     */
    public int getMovetolastRow() {
        if (movetolastRow == -1) {
            throw new RuntimeException();
        }

        return movetolastRow;
    }

    /**
     * Return the last column move
     *
     * @return Last column move
     * @throws RuntimeException if there is no move.
     */
    public int getMovetolastCol() {
        if (movetolastCol == -1) {
            throw new RuntimeException();
        }

        return movetolastCol;
    }
}
