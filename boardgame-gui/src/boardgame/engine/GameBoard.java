package boardgame.engine;

/**
 * A GameBoard defines any game that uses a 2-dimensional GameBoard
 *
 * @param <data> The data that can be stored in the 2-dimensional GameBoard
 */
public abstract class GameBoard<data> {

    private Object[][] board;

    /**
     * Create a new GameBoard
     *
     * @param rowSize Row size
     * @param colSize Columns size
     * @throws IllegalArgumentException if the dimension sizes are invalid
     */
    GameBoard(int rowSize, int colSize) {
        if (rowSize <= 0 || colSize <= 0) {
            throw new IllegalArgumentException();
        }

        board = new Object[rowSize][colSize];

    }

    /**
     * Return the number of rows
     *
     * @return Number of rows
     */
    public int getRowSize() {
        return board.length;
    }

    /**
     * Return the number of columns
     *
     * @return Number of columns
     */
    public int getColumnSize() {
        return board[0].length;
    }

    /**
     * Set a value at a GameBoard
     *
     * @param row Target row
     * @param col Target column
     * @param element Element to place
     * @throws IllegalArgumentException if coordinates are invalid
     */
    void setValue(int row, int col, data element) {
        if (row < 0 || row >= board.length || col < 0 || col >= board[0].length) {
            throw new IllegalArgumentException();
        }

        board[row][col] = element;
    }

    /**
     * Get a value at a GameBoard
     *
     * @param row Target row
     * @param col Target column
     * @return Content of GameBoard at row and column
     * @throws IllegalArgumentException if coordinates are invalid
     */
    public data getValue(int row, int col) {
        if (row < 0 || row >= board.length || col < 0 || col >= board[0].length) {
            throw new IllegalArgumentException();
        }

        if (board[row][col] == null) {
            return null;
        }

        return (data) board[row][col];
    }

    /**
     * Return a string representation of the GameBoard
     *
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        for (int r = 0; r < getRowSize(); r++) {
            for (int c = 0; c < getColumnSize(); c++) {
                str.append("[").append(getValue(r, c).toString()).append("]");
            }

            str.append("\n");
        }

        return str.toString();
    }
}
